﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hacker : MonoBehaviour {

	enum Screen {MainMenu, GuessPassword, Win}
	Screen currentScreen = Screen.MainMenu;

	int levelIndex;
	int passwordIndex;
	string[][] passwordsByLevel = {
			new string[] {"banana", "pineapple", "orange"},
			new string[] {"desk", "stapler", "printer"},
			new string[] {"moonwalk", "deathray", "rocket"},
		};

	void ShowMainMenu(){
		this.currentScreen = Screen.MainMenu;
		Terminal.ClearScreen();
		Terminal.WriteLine("Let's stop EvilCorps evil activities!\nWhat do you want to hack into?\n");
		Terminal.WriteLine("Type 1 for the EvilCorp Factory");
		Terminal.WriteLine("Type 2 for the EvilCorp HQ");
		Terminal.WriteLine("Type 3 for the EvilCorp Satellite\n");
		Terminal.WriteLine("Type your selection: ");
	}

	// Use this for initialization
	void Start () {
		ShowMainMenu();
	}

	void OnUserInput(string input){
		if (input == "menu") {
			ShowMainMenu();
		} else if (this.currentScreen == Screen.MainMenu) {
			RunMainMenu(input);
		} else if (this.currentScreen == Screen.GuessPassword) {
			CheckPassword(input);
		} else if (this.currentScreen == Screen.Win) {
			ShowMainMenu();
		}
	}

	void RunMainMenu(string input) {
		string[] validLevelNumbers = {"1", "2", "3"};
		bool isValidLevel = Array.Exists(
			validLevelNumbers, 
			levelNumber => levelNumber == input);

		if (isValidLevel) {
			StartValidLevel(int.Parse(input));
		} else if (input == "007") {
			// easter egg
			Terminal.WriteLine("Stop wasting time Mr. Bond");
		}
		else {
			print("ERROR: Invalid input");
		}
	}

	void StartValidLevel(int level) {
		this.levelIndex = level -1;
		int numberOfPasswordsForLevel = this.passwordsByLevel[this.levelIndex].Length;
		this.passwordIndex = UnityEngine.Random.Range(0, numberOfPasswordsForLevel);
		this.currentScreen = Screen.GuessPassword;
		AskForPassword();
	}

	void AskForPassword() {
		Terminal.ClearScreen();
		string password = this.passwordsByLevel[this.levelIndex][this.passwordIndex];
		Terminal.WriteLine("Enter password, hint " + password.Anagram());
	}

	void CheckPassword(string input) {
		string password = this.passwordsByLevel[this.levelIndex][this.passwordIndex];
		if (input == password) {
			this.currentScreen = Screen.Win;
			Terminal.WriteLine("! Hack successful, access granted !");
			Terminal.WriteLine("Press enter to return to main menu.");
		} else {
			Terminal.WriteLine("Access denied, try again:");
		}
	}
}
